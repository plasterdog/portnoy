<?php
/**
 * portnoy functions and definitions
 *
 * @package portnoy
 */
require_once( __DIR__ . '/inc/customizer-styles.php');
require_once( __DIR__ . '/inc/simplify-profiles.php');
require_once( __DIR__ . '/inc/widget-styling.php');
require_once( __DIR__ . '/inc/backend-experience.php');
require_once( __DIR__ . '/inc/global-fields.php');

//require_once( __DIR__ . '/inc/required-plugins.php');
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'portnoy_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function portnoy_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on portnoy, use a find and replace
	 * to change 'portnoy' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'portnoy', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'portnoy' ),
		'footer' => __( 'Footer Menu', 'portnoy' ),
	) );
	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'portnoy_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );


	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );
}
endif; // portnoy_setup
add_action( 'after_setup_theme', 'portnoy_setup' );

//https://wordpress.stackexchange.com/questions/250349/how-to-remove-menus-section-from-wordpress-theme-customizer

function mytheme_customize_register( $wp_customize ) {
  //All our sections, settings, and controls will be added here

  //$wp_customize->remove_section( 'title_tagline');
  $wp_customize->remove_panel( 'nav_menus');
  $wp_customize->remove_panel( 'widgets');
  //$wp_customize->remove_section( 'static_front_page');
}
add_action( 'customize_register', 'mytheme_customize_register',50 );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function portnoy_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Archive & Post Sidebar', 'portnoy' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );

        register_sidebar( array(
        'name'          => __( 'Regular Page Sidebar', 'portnoy' ),
        'id'            => 'sidebar-3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );


 }
add_action( 'widgets_init', 'portnoy_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function portnoy_scripts() {

	wp_enqueue_style( 'reset-styles', get_template_directory_uri() . '/css/reset-styles.css',false,'1.1','all');

	wp_enqueue_style( 'portnoy-style', get_stylesheet_uri() );

	wp_enqueue_script( 'portnoy-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'portnoy-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'portnoy_scripts' );

/**
 * Implement the Custom Header feature. - JMC ACTIVATED
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
//JMC => additional image size
function pw_add_image_sizes() {
    add_image_size( 'large-thumb', 300, 300, true );

}
add_action( 'init', 'pw_add_image_sizes' );
 
function pw_show_image_sizes($sizes) {
    $sizes['large-thumb'] = __( 'Custom Thumb', 'portnoy' );

 
    return $sizes;
}
add_filter('image_size_names_choose', 'pw_show_image_sizes');

 /* JMC --* Enable support for Post Thumbnails	 */
	add_theme_support( 'post-thumbnails' );

/* JMC because it is required */
	add_theme_support( 'title-tag' );

//JMC remove anchor link from more tag

function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');

//JMC-https://carriedils.com/add-editor-style/ <= will apply the stylesheet to the editor view

add_action( 'init', 'cd_add_editor_styles' );
/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function cd_add_editor_styles() {
 add_editor_style( get_stylesheet_uri() );
}

/** https://gist.github.com/scottparry/be66973877ea42b8531a01f5a23580b9
  * Enqueue custom fonts using protocol relative URL.
  *
  * Syntax: wp_enqueue_style( $handle, $src, $deps, $ver, $media );
  * Ensure $handle is unique to prevent conflicts with plugins
  *
  * Note(s): The pipe (|) operator is used to load multiple typefaces in a single call. We also only load the weights we want   * by comma seperating them, instead of loading every available weight.
  */
function theme_prefix_fonts() 
{

    wp_enqueue_style( 'theme-prefix-fonts', "//fonts.googleapis.com/css?family=Nunito:200,400,400i,600,800 | //fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,800", '', '1.0.0', 'screen' );
}
add_action( 'wp_enqueue_scripts', 'theme_prefix_fonts' );

//control width of Gutenberg editor https://github.com/WordPress/gutenberg/issues/6064
function PDOG_admin_css() {
echo '<style type="text/css">
.wp-block { max-width: 1200px; }
</style>';
}
add_action('admin_head', 'PDOG_admin_css');



// Adds support for editor color palette - https://www.billerickson.net/code/color-palette-setup-in-gutenberg/

add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'Red', 'portnoy' ),
		'slug'  => 'pdog-red',
		'color'	=> '#b12328',
	),
	array(
		'name'  => __( 'Yellow', 'portnoy' ),
		'slug'  => 'pdog-yellow',
		'color' => '#ffcc00',
       ),
	array(
		'name'  => __( 'Blue', 'portnoy' ),
		'slug'  => 'pdog-blue',
		'color' => '#006699',
	),	
	array(
		'name'  => __( 'Orange','portnoy' ),
		'slug'  => 'pdog-orange',
		'color' => '#ff8400',
	),	
	array(
		'name'  => __( 'Light Grey','portnoy' ),
		'slug'  => 'pdog-light-grey',
		'color' => '#e5e5e5',
	),		
	array(
		'name'  => __( 'Grey','portnoy' ),
		'slug'  => 'pdog-grey',
		'color' => '#898787',
	),	
	array(
		'name'  => __( 'Dark Grey','portnoy' ),
		'slug'  => 'pdog-dark-grey',
		'color' => '#333333',
	),	
	array(
		'name'  => __( 'Light Blue','portnoy' ),
		'slug'  => 'pdog-light-blue',
		'color' => '#05b9fc',
	),	
	array(
		'name'  => __( 'Dark Green','portnoy' ),
		'slug'  => 'pdog-dark-green',
		'color' => '#405b24',
	),		
	array(
		'name'  => __( 'Light Green','portnoy' ),
		'slug'  => 'pdog-light-green',
		'color' => '#05c3af',
	),	
	array(
		'name'  => __( 'Black','portnoy' ),
		'slug'  => 'pdog-black',
		'color' => '#000000',
	),		
	array(
		'name'  => __( 'White','portnoy' ),
		'slug'  => 'pdog-white',
		'color' => '#ffffff',
	),			
) );
