<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package portnoy
 */


get_header(); ?>
<?php if ( get_field( 'page_hero_image' ) ): ?>
<div id="hero-top">   
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/> 
</div>
<?php endif; ?> 
    <?php if ( !get_field( 'page_hero_image' ) ): ?>
        <div id="hero-top">
          <img src="<?php echo get_stylesheet_directory_uri() ?>/images/DefaultHeader.jpg" />

        </div>
      <?php endif; ?>

 <div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php if( get_field('show_title') == 'show' ): ?>

					<?php if( get_field('portnoy_alternate_title') ): ?>
					<h1><?php the_field('portnoy_alternate_title'); ?></h1>
					<?php endif; ?>

					<?php if(! get_field('portnoy_alternate_title') ): ?>
					<h1><?php the_title(); ?></h1>
					<?php endif; ?>

		<?php endif; ?><!-- the select clause -->

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'portnoy' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'portnoy' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	
	<div class="clear" style="height:2em;"></div>

<?php get_footer(); ?>
